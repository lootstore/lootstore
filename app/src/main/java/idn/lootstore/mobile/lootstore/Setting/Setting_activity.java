package idn.lootstore.mobile.lootstore.Setting;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.AppCompatTextView;
import android.widget.CompoundButton;
import android.widget.Switch;

import idn.lootstore.mobile.lootstore.R;

public class Setting_activity extends AppCompatActivity {
    Switch switchMode;
    SharedPreferences sharedPreferences;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        sharedPreferences = getSharedPreferences("SETTING",MODE_PRIVATE);
        if (AppCompatDelegate.getDefaultNightMode()== AppCompatDelegate.MODE_NIGHT_YES || sharedPreferences.getBoolean("darkMode",false)){
            setTheme(R.style.darktheme);
        }else {
            setTheme(R.style.AppTheme);
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        switchMode = findViewById(R.id.switchMode);
        switchMode.setChecked(sharedPreferences.getBoolean("darkMode",false));
        if (AppCompatDelegate.getDefaultNightMode()==AppCompatDelegate.MODE_NIGHT_YES){
            switchMode.setChecked(true);
        }
        switchMode.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
                }else {
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);

                }
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putBoolean("darkMode",isChecked);
                editor.apply();
                editor.commit();
                restartApp();
            }
        });

    }

    public void restartApp(){
//        Intent i = new Intent(getApplicationContext(), Setting_activity.class);
//        startActivity(i);
//        finish();
        recreate();
    }
}
