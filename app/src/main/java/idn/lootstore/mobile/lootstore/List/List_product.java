package idn.lootstore.mobile.lootstore.List;

/**
 * Created by GL552VW on 30/04/2019.
 */

public class List_product {
        private String nama, tahun, harga, seating, engine, power, powerStearing, driverAribag, antiLockbreakingSystem, namaPenjual, noHp, email;
        private String gambar;
    public List_product(String nama, String tahun, String harga, String seating, String engine, String power, String powerStearing, String driverAribag, String antiLockbreakingSystem, String namaPenjual, String noHp, String email, String gambar) {
        this.nama = nama;
        this.tahun = tahun;
        this.harga = harga;
        this.seating = seating;
        this.engine = engine;
        this.power = power;
        this.powerStearing = powerStearing;
        this.driverAribag = driverAribag;
        this.antiLockbreakingSystem = antiLockbreakingSystem;
        this.namaPenjual = namaPenjual;
        this.noHp = noHp;
        this.email = email;
        this.gambar = gambar;
    }

    public String getGambar() {
        return gambar;
    }

    public String getNama() {
        return nama;
    }

    public String getTahun() {
        return tahun;
    }

    public String getHarga() {
        return harga;
    }

    public String getSeating() {
        return seating;
    }

    public String getEngine() {
        return engine;
    }

    public String getPower() {
        return power;
    }

    public String getPowerStearing() {
        return powerStearing;
    }

    public String getDriverAribag() {
        return driverAribag;
    }

    public String getAntiLockbreakingSystem() {
        return antiLockbreakingSystem;
    }

    public String getNamaPenjual() {
        return namaPenjual;
    }

    public String getNoHp() {
        return noHp;
    }

    public String getEmail() {
        return email;
    }
}
