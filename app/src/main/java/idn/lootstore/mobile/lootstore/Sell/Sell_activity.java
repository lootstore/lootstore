package idn.lootstore.mobile.lootstore.Sell;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatDelegate;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.UUID;

import idn.lootstore.mobile.lootstore.Account.Account_activity;
import idn.lootstore.mobile.lootstore.Main_activity;
import idn.lootstore.mobile.lootstore.R;

public class Sell_activity extends AppCompatActivity {
    EditText et_nama,et_tahun,et_harga,totalSeat,engine,power,powerStearing,driverAir,antiLock,namaPenjual,noHP,email;
    ImageView imgProduct;
    Bitmap bitmap;
    private static final int CAM_Req = 1;
    String ID,url;
    FirebaseUser user;
    DatabaseReference dbProduct, dbKey;
    private Uri filePath;


    FirebaseStorage storage;
    StorageReference storageReference, retrieveImgStorage;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sell);

        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();
        user = FirebaseAuth.getInstance().getCurrentUser();
        dbKey = FirebaseDatabase.getInstance().getReference("product");

        String uid = dbKey.push().getKey();
        System.out.println("uid"+uid);


        dbProduct = FirebaseDatabase.getInstance().getReference("product").child(user.getUid()).child(uid);

        imgProduct = findViewById(R.id.img_product);
        et_nama = findViewById(R.id.namaProduk);
        et_tahun = findViewById(R.id.tahunProduct);
        et_harga = findViewById(R.id.hargaProduct);
        totalSeat = findViewById(R.id.totalSeat);
        engine = findViewById(R.id.engine);
        power = findViewById(R.id.powerEg);
        powerStearing = findViewById(R.id.powerStearing);
        driverAir = findViewById(R.id.driverAir);
        antiLock = findViewById(R.id.antiLock);
        namaPenjual = findViewById(R.id.namaPenjual);
        noHP = findViewById(R.id.noHp);
        email = findViewById(R.id.email);




        Intent intent = getIntent();
        //decode array bitmap
        byte[] byteArray = getIntent().getByteArrayExtra("bytearray");
        bitmap = BitmapFactory.decodeByteArray(byteArray, 0 ,byteArray.length);
        imgProduct.setImageBitmap(bitmap);

        String Filepath = intent.getStringExtra("filepath");
        filePath = Uri.parse(Filepath);
        System.out.println("file"+filePath);



    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        SharedPreferences sharedPreferences = getSharedPreferences("SETTING",MODE_PRIVATE);
        if (AppCompatDelegate.getDefaultNightMode()== AppCompatDelegate.MODE_NIGHT_YES || sharedPreferences.getBoolean("darkMode",false)){
            setTheme(R.style.darktheme);
        }else {
            setTheme(R.style.AppTheme);
        }
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAM_Req){
            Bundle extras = data.getExtras();
            bitmap = (Bitmap)extras.get("data");
            imgProduct.setImageBitmap(bitmap);

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(this, Main_activity.class));
        finish();
    }

    public void uploadData(View view) {
        System.out.println(ID);
        if (filePath != null) {
            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setTitle("Uploading...");
            progressDialog.show();

            final StorageReference ref = storageReference.child("images/").child(user.getUid()).child(UUID.randomUUID().toString());
            ref.putFile(filePath);


            ref.putFile(filePath)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            progressDialog.dismiss();
                            Toast.makeText(Sell_activity.this, "Berhasil simpan", Toast.LENGTH_SHORT).show();

                            ref.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(Uri uri) {
                                    url = uri.toString();
                                    dbProduct.child("image").setValue(url);
                                    System.out.println("file" + filePath);
                                }
                            }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    Toast.makeText(Sell_activity.this, "Failed to get URL", Toast.LENGTH_SHORT).show();
                                }
                            });

                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            progressDialog.dismiss();
                            Toast.makeText(Sell_activity.this, "Failed " + e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            double progress = (100.0 * taskSnapshot.getBytesTransferred() / taskSnapshot
                                    .getTotalByteCount());
                            progressDialog.setMessage("Uploaded " + (int) progress + "%");
                        }
                    });
        }

        dbProduct.child("anti-lock").setValue(antiLock.getText().toString());
        dbProduct.child("driverAir").setValue(driverAir.getText().toString());
        dbProduct.child("email").setValue(email.getText().toString());
        dbProduct.child("engine").setValue(engine.getText().toString());
        dbProduct.child("harga").setValue(et_harga.getText().toString());
        dbProduct.child("nama").setValue(et_nama.getText().toString());
        dbProduct.child("namaPenjual").setValue(namaPenjual.getText().toString());
        dbProduct.child("noHp").setValue(noHP.getText().toString());
        dbProduct.child("power").setValue(power.getText().toString());
        dbProduct.child("powerStearing").setValue(powerStearing.getText().toString());
        dbProduct.child("seating").setValue(totalSeat.getText().toString());
        dbProduct.child("tahun").setValue(et_tahun.getText().toString());

        startActivity(new Intent(this, Main_activity.class));
        Toast.makeText(this, "Upload successfully", Toast.LENGTH_SHORT).show();



    }
}
