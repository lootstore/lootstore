package idn.lootstore.mobile.lootstore;


import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.IOException;
import java.io.InputStream;

import idn.lootstore.mobile.lootstore.Account.Account_activity;
import idn.lootstore.mobile.lootstore.Register.Register_activity;
import idn.lootstore.mobile.lootstore.Setting.Setting_activity;


/**
 * A simple {@link Fragment} subclass.
 */
public class Fragment_profile extends Fragment {

    View view;
    CardView logout,account;
    LinearLayout linearLayoutLogin;

    TextView txt_register, usernameAccount;
    EditText et_emaiL, et_password;
    Button btnLogin;

    FrameLayout frameLogout, frameSetting;
    Uri uri;
    DatabaseReference dbProfile, dbImage;
    FirebaseAuth mAuth;
    FirebaseUser user;
    ImageView imgProfile;
    private int SETTING = 4732;

    public Fragment_profile() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_profile, container, false);
        ((AppCompatActivity)getActivity()).getSupportActionBar().hide();

        imgProfile = view.findViewById(R.id.img_profile);


        mAuth = FirebaseAuth.getInstance();
        user  = mAuth.getCurrentUser();


        if (user != null) {
            dbImage = FirebaseDatabase.getInstance().getReference("Users").child(user.getUid()).child("image");

            dbImage.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                uri = Uri.parse(dataSnapshot.getValue().toString());
                    System.out.println(uri);
                    Glide.with(getActivity()).load(uri).into(imgProfile);

                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });

            dbProfile = FirebaseDatabase.getInstance().getReference("Users").child(user.getUid());
            dbProfile.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    usernameAccount.setText(dataSnapshot.child("username").getValue().toString());
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        }
        txt_register = view.findViewById(R.id.txt_registerAccount);
        et_emaiL     = view.findViewById(R.id.et_email);
        et_password  = view.findViewById(R.id.et_pass);
        btnLogin     = view.findViewById(R.id.btn_login);

        usernameAccount = view.findViewById(R.id.name_profile);

        logout      = view.findViewById(R.id.cardLogout);
        account     = view.findViewById(R.id.cardAccount);
        linearLayoutLogin = view.findViewById(R.id.LinearLogin);

        frameLogout  = view.findViewById(R.id.frameLogout);
        frameSetting = view.findViewById(R.id.frameSetting);

        if (user == null){
            linearLayoutLogin.setVisibility(View.VISIBLE);
            account.setVisibility(View.INVISIBLE);
            logout.setVisibility(View.INVISIBLE);
        }else {
            linearLayoutLogin.setVisibility(View.INVISIBLE);
            account.setVisibility(View.VISIBLE);
            logout.setVisibility(View.VISIBLE);
        }

        account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), Account_activity.class));
            }
        });


        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email,password;

                email        = et_emaiL.getText().toString();
                password     = et_password.getText().toString();

                System.out.println(email);
                System.out.println(password);

                mAuth.signInWithEmailAndPassword(email, password)
                        .addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if (task.isSuccessful()) {

                                    startActivity(new Intent(getContext(), Main_activity.class));
                                } else {
                                    // If sign in fails, display a message to the user.
                                    Toast.makeText(getContext(), "Invalid user", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
            }
        });

        frameLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (user != null){
                    mAuth.signOut();
                    startActivity(new Intent(getContext(), Main_activity.class));
                    getActivity().finish();
                }
            }
        });

        frameSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                startActivity(new Intent(getContext(), Setting_activity.class));
                startActivityForResult(new Intent(getContext(),Setting_activity.class),SETTING);
            }
        });

        txt_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), Register_activity.class));
            }
        });



        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode==SETTING){
            Toast.makeText(getContext(), "KENAAFragment", Toast.LENGTH_SHORT).show();
            getActivity().recreate();
        }
    }

}
