package idn.lootstore.mobile.lootstore.Register;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatDelegate;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import idn.lootstore.mobile.lootstore.Main_activity;
import idn.lootstore.mobile.lootstore.R;

public class Register_activity extends AppCompatActivity {
    String email,password;
    EditText et_email, et_password, et_username, et_mobile;

    private FirebaseAuth mAuth;
    private DatabaseReference dbRegister;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        SharedPreferences sharedPreferences = getSharedPreferences("SETTING",MODE_PRIVATE);
        if (AppCompatDelegate.getDefaultNightMode()== AppCompatDelegate.MODE_NIGHT_YES || sharedPreferences.getBoolean("darkMode",false)){
            setTheme(R.style.darktheme);
        }else {
            setTheme(R.style.AppTheme);
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_activity);


        et_email = findViewById(R.id.et_email);
        et_password = findViewById(R.id.et_pass);
        et_username = findViewById(R.id.et_username);
        et_mobile = findViewById(R.id.et_noTelp);

        mAuth = FirebaseAuth.getInstance();
        dbRegister = FirebaseDatabase.getInstance().getReference("Users");

    }

    public void registerAccount(View view) {
        email = et_email.getText().toString();
        password = et_password.getText().toString();

        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            FirebaseUser user = mAuth.getCurrentUser();
                            dbRegister.child(user.getUid()).child("email").setValue(email);
                            dbRegister.child(user.getUid()).child("noTelp").setValue(et_mobile.getText().toString());
                            dbRegister.child(user.getUid()).child("username").setValue(et_username.getText().toString());
                            startActivity(new Intent(Register_activity.this, Main_activity.class));
                        } else {
                            // If sign in fails, display a message to the user.
                            Toast.makeText(Register_activity.this, "Register failed", Toast.LENGTH_SHORT).show();
                        }

                        // ...
                    }
                });

    }
}
