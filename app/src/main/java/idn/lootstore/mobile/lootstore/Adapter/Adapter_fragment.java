package idn.lootstore.mobile.lootstore.Adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by GL552VW on 30/04/2019.
 */

public class Adapter_fragment extends FragmentPagerAdapter{
    List<Fragment> fragmentList = new ArrayList<>();

    public Adapter_fragment(FragmentManager fm) {
        super(fm);
    }

    public void addFragment(Fragment fm){
        fragmentList.add(fm);
    }

    @Override
    public Fragment getItem(int i) {
        return fragmentList.get(i);
    }

    @Override
    public int getCount() {
        return fragmentList.size();
    }
}
