package idn.lootstore.mobile.lootstore.Adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import idn.lootstore.mobile.lootstore.Detail.Detail_activity;
import idn.lootstore.mobile.lootstore.List.List_product;
import idn.lootstore.mobile.lootstore.R;

/**
 * Created by GL552VW on 30/04/2019.
 */

public class Adapter_product extends RecyclerView.Adapter<Adapter_product.ViewHolder> implements Filterable{
    private ArrayList<List_product> list_products;
    private ArrayList<List_product> list_products2;
    private Context mContext;

    @Override
    public Filter getFilter() {
        return filterProduct;
    }

    private Filter filterProduct = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<List_product> filteredList = new ArrayList<>();

            if (constraint == null || constraint.length() == 0){
                filteredList.addAll(list_products2);
            }else {
                String filterPattern = constraint.toString().toLowerCase().trim();

                for (List_product item : list_products2){
                    if (item.getNama().toLowerCase().contains(filterPattern)){
                        filteredList.add(item);
                    }
                }
            }

            FilterResults results = new FilterResults();
            results.values = filteredList;
            return  results;

        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            list_products.clear();
            list_products.addAll((List) results.values);
            notifyDataSetChanged();
        }
    };




    public static class ViewHolder extends RecyclerView.ViewHolder{
        private TextView namaProduct, hargaProduct;
        private ImageView imgProduct;

        String tahun,seating,engine,power,powerStearing,driverAir,antiLock,namaPenjual,noHp,email;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            this.namaProduct = itemView.findViewById(R.id.txt_namaProduct);
            this.hargaProduct = itemView.findViewById(R.id.txt_price);
            this.imgProduct = itemView.findViewById(R.id.img_product);
        }
    }

    public Adapter_product(ArrayList<List_product> list_products, Context context) {
        this.list_products = list_products;
        this.mContext = context;
        list_products2 = new ArrayList<>(list_products);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull final ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_product, viewGroup, false);
        final ViewHolder viewHolder = new ViewHolder(v);

        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, Detail_activity.class);
                intent.putExtra("nama", viewHolder.namaProduct.getText().toString());
                intent.putExtra("harga", viewHolder.hargaProduct.getText().toString());
                intent.putExtra("tahun", viewHolder.tahun.toString());
                intent.putExtra("antiLock", viewHolder.antiLock.toString());
                intent.putExtra("driverAir", viewHolder.driverAir.toString());
                intent.putExtra("email", viewHolder.email.toString());
                intent.putExtra("engine", viewHolder.engine.toString());
                intent.putExtra("power", viewHolder.power.toString());
                intent.putExtra("powerStearing", viewHolder.powerStearing.toString());
                intent.putExtra("namaPenjual", viewHolder.namaPenjual.toString());
                intent.putExtra("noHp", viewHolder.noHp.toString());
                intent.putExtra("seating", viewHolder.seating.toString());
                mContext.startActivity(intent);
            }
        });

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
            List_product list_product = list_products.get(i);

//            viewHolder.imgProduct.setImageResource(list_product.get);
            viewHolder.namaProduct.setText(list_product.getNama());
            viewHolder.hargaProduct.setText("Rp. "+list_product.getHarga());

            viewHolder.tahun = list_product.getTahun().toString();
            viewHolder.antiLock = list_product.getAntiLockbreakingSystem().toString();
            viewHolder.driverAir = list_product.getDriverAribag().toString();
            viewHolder.email = list_product.getEmail().toString();
            viewHolder.engine = list_product.getEngine().toString();
            viewHolder.power = list_product.getPower().toString();
            viewHolder.powerStearing = list_product.getPowerStearing().toString();
            viewHolder.namaPenjual = list_product.getNamaPenjual().toString();
            viewHolder.noHp = list_product.getNoHp().toString();
            viewHolder.seating = list_product.getSeating().toString();

            String img = list_product.getGambar().toString();
            Uri uri = Uri.parse(img);
            Glide.with(mContext).load(uri).into(viewHolder.imgProduct);

    }

    @Override
    public int getItemCount() {
        return list_products.size();
    }
}
