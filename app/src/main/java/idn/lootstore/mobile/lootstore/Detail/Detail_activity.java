package idn.lootstore.mobile.lootstore.Detail;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatDelegate;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import idn.lootstore.mobile.lootstore.R;

public class Detail_activity extends AppCompatActivity {
    TextView nama, tahun, harga, seating, engine, power, powerStearing, driverAribag, antiLockbreakingSystem, namaPenjual, noHp, email;
    ImageView imgProduct;
    String name, seller;

    DatabaseReference dbImg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        SharedPreferences sharedPreferences = getSharedPreferences("SETTING", MODE_PRIVATE);
        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES || sharedPreferences.getBoolean("darkMode", false)) {
            setTheme(R.style.darktheme);
        } else {
            setTheme(R.style.AppTheme);
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        getSupportActionBar().setTitle(getIntent().getStringExtra("nama"));
        imgProduct = findViewById(R.id.img_product);
        nama = findViewById(R.id.namaProduk);
        tahun = findViewById(R.id.tahunProduct);
        harga = findViewById(R.id.hargaProduct);
        seating = findViewById(R.id.totalSeat);
        engine = findViewById(R.id.totalSeat);
        power = findViewById(R.id.powerEg);
        powerStearing = findViewById(R.id.powerStearing);
        driverAribag = findViewById(R.id.driverAir);
        antiLockbreakingSystem = findViewById(R.id.antiLock);
        namaPenjual = findViewById(R.id.namaPenjual);
        noHp = findViewById(R.id.noHp);
        email = findViewById(R.id.email);

        dbImg = FirebaseDatabase.getInstance().getReference("product");

        Intent intent = getIntent();
        name = intent.getStringExtra("nama");
        seller = intent.getStringExtra("namaPenjual");

        nama.setText(name);
        tahun.setText(intent.getStringExtra("tahun"));
        harga.setText(intent.getStringExtra("harga"));
        seating.setText(intent.getStringExtra("seating"));
        engine.setText(intent.getStringExtra("engine"));
        power.setText(intent.getStringExtra("power"));
        powerStearing.setText(intent.getStringExtra("powerStearing"));
        driverAribag.setText(intent.getStringExtra("driverAir"));
        antiLockbreakingSystem.setText(intent.getStringExtra("antiLock"));
        namaPenjual.setText(seller);
        noHp.setText(intent.getStringExtra("noHp"));
        email.setText(intent.getStringExtra("email"));


            dbImg.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                        for (DataSnapshot dataSnapshot2 : dataSnapshot1.getChildren()) {
                            if (dataSnapshot2.child("nama").equals(name) || dataSnapshot2.child("namaPenjual").equals(seller)) {
                                Uri uri = Uri.parse(dataSnapshot2.child("image").getValue().toString());
                                Glide.with(Detail_activity.this).load(uri).into(imgProduct);
                                System.out.println("img" + dataSnapshot2.child("image").getValue());
                            }
                        }
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });


    }
}
