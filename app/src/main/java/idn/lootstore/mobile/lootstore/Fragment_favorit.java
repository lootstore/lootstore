package idn.lootstore.mobile.lootstore;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import idn.lootstore.mobile.lootstore.Adapter.Adapter_product;
import idn.lootstore.mobile.lootstore.List.List_product;


/**
 * A simple {@link Fragment} subclass.
 */
public class Fragment_favorit extends Fragment {
    View view;

    RecyclerView recyclerView;
    RecyclerView.Adapter adapter;
    RecyclerView.LayoutManager layoutManager;
    ArrayList<List_product> list_products;

    DatabaseReference dbHome;


    public Fragment_favorit() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view =  inflater.inflate(R.layout.fragment_favorit, container, false);

        recyclerView = view.findViewById(R.id.recyclerviewFav);
        list_products = new ArrayList<>();
        adapter = new Adapter_product(list_products, view.getContext());
        recyclerView.setAdapter(adapter);
        layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);


        dbHome = FirebaseDatabase.getInstance().getReference("product");

        dbHome.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                list_products.clear();
                for (DataSnapshot dataSnapshot2 : dataSnapshot.getChildren()){
                    for (DataSnapshot dataSnapshot1 : dataSnapshot2.getChildren()){
                        if (dataSnapshot1.child("image").getValue() != null) {
                            list_products.add(new List_product(
                                    dataSnapshot1.child("nama").getValue().toString(),
                                    dataSnapshot1.child("tahun").getValue().toString(),
                                    dataSnapshot1.child("harga").getValue().toString(),
                                    dataSnapshot1.child("seating").getValue().toString(),
                                    dataSnapshot1.child("engine").getValue().toString(),
                                    dataSnapshot1.child("power").getValue().toString(),
                                    dataSnapshot1.child("powerStearing").getValue().toString(),
                                    dataSnapshot1.child("driverAir").getValue().toString(),
                                    dataSnapshot1.child("anti-lock").getValue().toString(),
                                    dataSnapshot1.child("namaPenjual").getValue().toString(),
                                    dataSnapshot1.child("noHp").getValue().toString(),
                                    dataSnapshot1.child("email").getValue().toString(),
                                    dataSnapshot1.child("image").getValue().toString()
                            ));
                        }
                    }
                }
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        return view;
    }

}
