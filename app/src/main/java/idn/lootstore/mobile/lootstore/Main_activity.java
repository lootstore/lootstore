package idn.lootstore.mobile.lootstore;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import idn.lootstore.mobile.lootstore.Adapter.Adapter_fragment;
import idn.lootstore.mobile.lootstore.Sell.Sell_activity;

public class Main_activity extends AppCompatActivity {

    BottomNavigationView bottomNavigationMain;
    Fragment_home fragment_home;
    Fragment_sell fragment_sell;
    Fragment_favorit fragment_favorit;
    Fragment_profile fragment_profile;
    ViewPager viewPager;
    Adapter_fragment adapter_fragment;
    private final int PICK_IMAGE_REQUEST = 71;

    private static final int CAM_Req = 1;
    Bitmap bitmap;
    Uri filePath;

    int currentPosition = 4;
    private int SETTING = 4732;


    FirebaseAuth mAuth;
    FirebaseUser mUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        SharedPreferences sharedPreferences = getSharedPreferences("SETTING",MODE_PRIVATE);
        if (AppCompatDelegate.getDefaultNightMode()== AppCompatDelegate.MODE_NIGHT_YES || sharedPreferences.getBoolean("darkMode",false)){
            Toast.makeText(this, "DARKKKK", Toast.LENGTH_SHORT).show();
            setTheme(R.style.darktheme);
        }else {
            setTheme(R.style.AppTheme);
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mAuth = FirebaseAuth.getInstance();
        mUser = mAuth.getCurrentUser();

        bottomNavigationMain = findViewById(R.id.bottomnavigationMain);
        fragment_home = new Fragment_home();
        fragment_sell = new Fragment_sell();
        fragment_favorit = new Fragment_favorit();
        fragment_profile = new Fragment_profile();
        viewPager = findViewById(R.id.viewPagerMain);

        adapter_fragment = new Adapter_fragment(getSupportFragmentManager());
        adapter_fragment.addFragment(fragment_home);
        adapter_fragment.addFragment(fragment_sell);
        adapter_fragment.addFragment(fragment_favorit);
        adapter_fragment.addFragment(fragment_profile);

        viewPager.setAdapter(adapter_fragment);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                bottomNavigationMain.getMenu().getItem(i).setChecked(true);
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });

        bottomNavigationMain.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                getSupportActionBar().show();
                switch (menuItem.getItemId()){
                    case R.id.home_nav:
                        viewPager.setCurrentItem(0);
                        currentPosition = 0;
                        return true;
                    case R.id.sell_nav:
                        if (mUser!= null){
                            Intent intent = new Intent();
                            intent.setType("image/*");
                            intent.setAction(Intent.ACTION_GET_CONTENT);
                            startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
                        }else {
                            bottomNavigationMain.setSelectedItemId(R.id.profile_nav);
                        }
                        return true;

                    case R.id.favorit_nav:
                        viewPager.setCurrentItem(2);
                        currentPosition = 2;
                        return true;
                    case R.id.profile_nav:
                        getSupportActionBar().hide();
                        viewPager.setCurrentItem(3);
                        currentPosition = 3;
                        return true;
                    default:
                        return false;
                }
            }
        });


    }

    public void getCamera(){
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(intent, CAM_Req);
        }else {
            startActivity(new Intent(Main_activity.this, Main_activity.class));
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK
                && data != null && data.getData() != null) {
            filePath = data.getData();
            String file = String.valueOf(filePath);
            System.out.println("fil" + filePath);
            System.out.println("fi"+file);
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                ByteArrayOutputStream bStream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, bStream);
                byte[] byteArray = bStream.toByteArray();

                Intent intent = new Intent(this, Sell_activity.class);
                intent.putExtra("bytearray", byteArray);
                intent.putExtra("filepath", file);
                startActivity(intent);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    @Override
    public void onBackPressed() {
        if (bottomNavigationMain.getSelectedItemId() != R.id.home_nav){
            bottomNavigationMain.setSelectedItemId(R.id.home_nav);
        }
        else {
            finish();
        }
    }

//
//    public Uri getImageUri(Context inContext, Bitmap inImage) {
//        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
//        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
//        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
//        return Uri.parse(path);
//    }
//
//    public String getRealPathFromURI(Uri uri) {
//        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
//        cursor.moveToFirst();
//        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
//        return cursor.getString(idx);
//    }
}


